// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.

let num1 = +prompt("input first number");

let operation = prompt("input +,-,*,/");
let num2 = +prompt("input second number");

// function sum(num1, num2) {
//   if (operation == "+") {
//     console.log(num1 + num2);
//   }
// }
// sum(num1, num2);

// function minus(num1, num2) {
//   if (operation == "-") {
//     console.log(num1 - num2);
//   }
// }
// minus(num1, num2);

// function division(num1, num2) {
//   if (operation == "/") {
//     console.log(num1 / num2);
//   }
// }
// division(num1, num2);

// function multiple(num1, num2) {
//   if (operation == "*") {
//     console.log(num1 * num2);
//   }
// }
// multiple(num1, num2);
let result;

switch (operation) {
  case "+":
    result = num1 + num2;
    break;
  case "-":
    result = num1 - num2;
    break;
  case "/":
    if (num2 !== 0) {
      result = num1 / num2;
    } else {
      console.log("Error: Division by zero");
    }
    break;
  default:
    console.log("Invalid operation");

  case "*":
    result = num1 * num2;
    break;
}
if (result !== undefined) {
  console.log(result);
}
